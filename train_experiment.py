# version 1.1

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import KFold

from neural_net import NeuralNetwork
from operations import *

def load_dataset(csv_path, target_feature):
    dataset = pd.read_csv(csv_path)
    t = np.expand_dims(dataset[target_feature].to_numpy().astype(np.float), axis=1)
    X = dataset.drop([target_feature], axis=1)
    return X, t

def train_evaluate_dataset(csv_path, target_feature, folds = 5, epochs = 1000, learning_rate = 0.01):
    f = open(csv_path + "_output.txt", "w")
    f.write ("32 32 16 1 ReLU ReLU ReLU Sigmoid \n")
    X, y = load_dataset("data/" + csv_path + ".csv", target_feature)
    kf = KFold(n_splits = folds, shuffle = True)
    kf.get_n_splits(X)
    accuracies = np.zeros(folds)
    epoch_losses_all = np.zeros((folds, epochs))
    i = 0
    for train_index, test_index in kf.split(X):
        print("fold {0}".format(i))
        n_features = X.shape[1]
        net = NeuralNetwork(n_features, [32,32,16,1], [ReLU(), ReLU(), ReLU(), Sigmoid()], CrossEntropy(), learning_rate=learning_rate)
        X_train = X.loc[train_index,]
        X_test = X.loc[test_index]
        y_train = y[train_index]
        y_test = y[test_index]
        trained_W, epoch_losses = net.train(X_train, y_train, epochs)
        accuracy_val = net.evaluate(X_test, y_test, accuracy)
        print("accuracy: ", accuracy_val)
        accuracies[i] = accuracy_val
        epoch_losses_all[i] = np.array(epoch_losses)
        i = i + 1
    print(accuracies)
    f.write(np.array2string(accuracies) + "\n")
    print("average accuracy: " , np.average(accuracies))
    f.write("average accuracy: " + np.array2string(np.average(accuracies)) + "\n")
    print("accuacy standard deviation: ", np.std(accuracies))
    f.write("accuacy standard deviation: " + np.array2string(np.std(accuracies)) + "\n")
    print("average epoch losses: ", np.average(epoch_losses_all, 0))
    f.write("average epoch losses: " + np.array2string(np.average(epoch_losses_all, 0)) + "\n")
    plt.plot(np.arange(0, epochs), epoch_losses)
    plt.title(csv_path)
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.savefig(csv_path + "_plot.png")
    plt.clf()
    f.close()



train_evaluate_dataset("banknote_authentication", "target", 5, 1000, 0.01)
train_evaluate_dataset("wine_quality", "quality", 5, 1000, 0.01)


    
# X, y = load_dataset("data/banknote_authentication.csv", "target")
# kf = KFold(n_splits = 5)
# kf.get_n_splits(X)
# print(X.loc[[1, 2]])
# for train_index, test_index in kf.split(X):
#     n_features = X.shape[1]
#     X_train = X[train_index]


# X, y = load_dataset("data/banknote_authentication.csv", "target")
# #X, y = load_dataset("data/wine_quality.csv", "quality")

# n_features = X.shape[1]
# net = NeuralNetwork(n_features, [32,32,16,1], [ReLU(), ReLU(), ReLU(), Sigmoid()], CrossEntropy(), learning_rate=0.01)
# epochs = 1

# test_split = 0.1
# X_train = X[:int((1 - test_split) * X.shape[0])]
# X_test = X[int((1 - test_split) * X.shape[0]):]
# y_train = y[:int((1 - test_split) * y.shape[0])]
# y_test = y[int((1 - test_split) * y.shape[0]):]

# print(X_train)
# print(y_train)

# print(len(X_train))
# print(np.array(X_train))

# trained_W, epoch_losses = net.train(X_train, y_train, epochs)
# print("Accuracy on test set: {}".format(net.evaluate(X_test, y_test, accuracy)))

# plt.plot(np.arange(0, epochs), epoch_losses)
# plt.show()

